//
window.callWhenReadyToGo = function(state) {
    if (state == 0)
        console.log('There are no ajax calls!');
    if (state == 1)
        console.log('All ajax calls completed!');
    if (state == 2)
        console.log('Some ajax calls timed out!');
};

!function () {
            window.XMLHttpRequestOpen = XMLHttpRequest.prototype.open;//override XHR init
            //we want the callback executed once per instructions - if even one call times out then fail.
            var hasTimedOut = false;
            window.xhr_open_cnt = 0; //number of XHR Rqs.
            var timer_check_cnt = 0; //number of times the Request timer has ended and xhr call is DONE.
            var timeOut = 5000;//Arbitary time out period.

            XMLHttpRequest.prototype.open = function (method, url, is_async) {
                // set a timer after the OPEN and test to see if the call makes it past 
                // LOADING or DONE within a specified time period.
                xhr_open_cnt++ //For each OPEN increment rq counter.
                
                this.addEventListener("readystatechange", function () {
                    if (this.readyState === 1) {//xhr open called
                        // If any ajax call takes longer than {timeOut} seconds then execute callback once.
                        setTimeout(function() {
                            //Check each call with a timer function to see if completed. Make sure there is one check per call before calling the callback function.
                            timer_check_cnt++;
                            
                            //If still loading then at least one xhr call has timed out so page does not appear to be loading.
                            if (this.readyState !== 4) {
                                hasTimedOut = true;
                            }    
                            //Once all other timers have completed call the callback once.
                            if (timer_check_cnt === xhr_open_cnt && hasTimedOut === false) {
                                window.callWhenReadyToGo(1);
                            }
                            else if (timer_check_cnt === xhr_open_cnt && hasTimedOut === true) {
                                window.callWhenReadyToGo(2);
                            }
                        }.bind(this), timeOut);//all ajax calls on page expected to load within 5 seconds
                    }
                }, false);

                return XMLHttpRequestOpen.apply(this, [method, url, is_async]);
            }
        }();
