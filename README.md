# xhr_detect_ext

I spent some time looking at building a chrome extension. You can inject script at various points in the execution time of loading the DOM. In the case of "document_start", the files are injected after any files from css, but before any other DOM is constructed or any other script is run. This ensures 
that all XHR opens are counted in a page. Take a look at the ext directory for the mainfest, this directs when the script will be injected.